package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class DrawObjectsFormRecTest {

    DrawObjectsMSFormRecognizer command = new DrawObjectsMSFormRecognizer();

    String OriginalFile = "C:\\iqbot\\Document Samples\\Stock Documents\\Classifier_Training_Folder\\To_Sort\\AL.jpg";
    String OutputFile = "C:\\iqbot\\Document Samples\\Stock Documents\\Classifier_Training_Folder\\To_Sort\\AL2.jpg";

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){
        ArrayList<Value> Boxes = GetBoxes();
        return new Object[][]{
                {OriginalFile, OutputFile,Boxes}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String InputFilePath, String OutputFilePath, ArrayList<Value> Dicts) throws Exception {

        StringValue d = command.action(Dicts,InputFilePath, OutputFilePath,"#f55d42",4,"#f0f0f0","#5ecaeb");
        //System.out.println("DEBUG:"+d.toString());
        //assertEquals(d.toString(),Results);
        assertEquals(true,true);
    }

    public ArrayList<Value> GetBoxes(){
        ArrayList<Value> ListToPass = new ArrayList<Value>();

        // Box 1
        HashMap<String,Value> MyBox0 = new HashMap<String,Value>();
        MyBox0.put("score",new StringValue("29.9"));
        MyBox0.put("y",new StringValue("371.0"));
        MyBox0.put("x",new StringValue("736.0"));
        MyBox0.put("keyname",new StringValue("EYES"));
        MyBox0.put("height",new StringValue("20.0"));
        MyBox0.put("text",new StringValue("BLO"));
        MyBox0.put("page",new StringValue("1"));
        MyBox0.put("type",new StringValue("string"));
        MyBox0.put("width",new StringValue("41.0"));
        DictionaryValue myDict0 = new DictionaryValue(MyBox0);
        ListToPass.add(myDict0);

        // Box 2
        HashMap<String,Value> MyBox1 = new HashMap<String,Value>();
        MyBox1.put("score",new StringValue("36.1"));
        MyBox1.put("y",new StringValue("59.0"));
        MyBox1.put("x",new StringValue("329.0"));
        MyBox1.put("keyname",new StringValue("LN"));
        MyBox1.put("height",new StringValue("57.0"));
        MyBox1.put("text",new StringValue("ALABAMA"));
        MyBox1.put("page",new StringValue("1"));
        MyBox1.put("type",new StringValue("string"));
        MyBox1.put("width",new StringValue("391.0"));
        DictionaryValue myDict1 = new DictionaryValue(MyBox1);
        ListToPass.add(myDict1);


       // Key = score, Value = 19.5
//        Key = X0, Value = 736.0
       // Key = keyname, Value = HAIR
        //Key = Height, Value = 19.0
        //Key = text, Value = BRO
        //Key = page, Value = 1



        return ListToPass;
    }
}

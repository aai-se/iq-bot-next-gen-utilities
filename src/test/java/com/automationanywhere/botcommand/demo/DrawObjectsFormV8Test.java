package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class DrawObjectsFormV8Test {

    DrawObjectsV8 command = new DrawObjectsV8();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){
        return new Object[][]{

                {"C:\\iqbot\\V8\\output\\17-395767_1.json","C:\\iqbot\\V8\\input\\17-395767_1.png","C:\\iqbot\\V8\\output\\17-395767_1_BOXES.png",""},
                {"C:\\iqbot\\V8\\output\\MDkwZGJiYjg4NDJkNmEyNg_3D_3D_1.json","C:\\iqbot\\V8\\input\\MDkwZGJiYjg4NDJkNmEyNg_3D_3D_1.png","C:\\iqbot\\V8\\output\\MDkwZGJiYjg4NDJkNmEyNg_3D_3D_1.png",""},

               {"C:\\iqbot\\V8\\output\\MDkwZGJiYjg4NDcxMDMxNw_3D_3D_1.json","C:\\iqbot\\V8\\input\\MDkwZGJiYjg4NDcxMDMxNw_3D_3D_1.png","C:\\iqbot\\V8\\output\\MDkwZGJiYjg4NDcxMDMxNw_3D_3D_1.png",""},

              {"C:\\iqbot\\V8\\output\\MDkwZGJiYjg4M2ZkYTdiMQ_3D_3D_1.json","C:\\iqbot\\V8\\input\\MDkwZGJiYjg4M2ZkYTdiMQ_3D_3D_1.png","C:\\iqbot\\V8\\output\\MDkwZGJiYjg4M2ZkYTdiMQ_3D_3D_1.png",""},
              {"C:\\iqbot\\V8\\output\\MDkwZGJiYjg4M2ZkYTdhZg_3D_3D_1.json","C:\\iqbot\\V8\\input\\MDkwZGJiYjg4M2ZkYTdhZg_3D_3D_1.png","C:\\iqbot\\V8\\output\\MDkwZGJiYjg4M2ZkYTdhZg_3D_3D_1.png",""},
              {"C:\\iqbot\\V8\\output\\MDkwZGJiYjg4M2VmZjRlOQ_3D_3D_1.json","C:\\iqbot\\V8\\input\\MDkwZGJiYjg4M2VmZjRlOQ_3D_3D_1.png","C:\\iqbot\\V8\\output\\MDkwZGJiYjg4M2VmZjRlOQ_3D_3D_1.png",""},
              {"C:\\iqbot\\V8\\output\\MDkwZGJiYjg4M2JiNGZkYg_3D_3D_1.json","C:\\iqbot\\V8\\input\\MDkwZGJiYjg4M2JiNGZkYg_3D_3D_1.png","C:\\iqbot\\V8\\output\\MDkwZGJiYjg4M2JiNGZkYg_3D_3D_1.png",""},


              {"C:\\iqbot\\V8\\output\\IMB7_1.json","C:\\iqbot\\V8\\input\\IMB7_1.png","C:\\iqbot\\V8\\output\\IMB7_1.png",""},
              {"C:\\iqbot\\V8\\output\\IBM5_1.json","C:\\iqbot\\V8\\input\\IBM5_1.png","C:\\iqbot\\V8\\output\\IBM5_1.png",""},
              {"C:\\iqbot\\V8\\output\\IBM4_1.json","C:\\iqbot\\V8\\input\\IBM4_1.png","C:\\iqbot\\V8\\output\\IBM4_1.png",""},
              {"C:\\iqbot\\V8\\output\\IBM3_1.json","C:\\iqbot\\V8\\input\\IBM3_1.png","C:\\iqbot\\V8\\output\\IBM3_1.png",""},

              {"C:\\iqbot\\V8\\output\\IBM2_1.json","C:\\iqbot\\V8\\input\\IBM2_1.png","C:\\iqbot\\V8\\output\\IBM2_1.png",""},
              {"C:\\iqbot\\V8\\output\\IBM1_1.json","C:\\iqbot\\V8\\input\\IBM1_1.png","C:\\iqbot\\V8\\output\\IBM1_1.png",""},
              {"C:\\iqbot\\V8\\output\\17-415778_1.json","C:\\iqbot\\V8\\input\\17-415778_1.png","C:\\iqbot\\V8\\output\\17-415778_1.png",""},
              {"C:\\iqbot\\V8\\output\\17-407449_1.json","C:\\iqbot\\V8\\input\\17-407449_1.png","C:\\iqbot\\V8\\output\\17-407449_1.png",""}

        };
    }

    @Test(dataProvider = "data")
    public void aTests(String JsonInputFile, String InputFilePath, String OutputFilePath, String Res) throws Exception {
        String JSONInput = Utils.FileToString(JsonInputFile);
        if(!JSONInput.equals("")){
            //System.out.println("DEBUG Processing content:["+JSONInput+"]");
            StringValue d = command.action(JSONInput,InputFilePath,OutputFilePath);
        }

    }
}

package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;

@Test(enabled=true)
public class JSONToCSVTest {

    JSONToCSV command = new JSONToCSV();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){
        return new Object[][]{

                {"C:\\iqbot\\V8\\output",""}

        };
    }

    @Test(dataProvider = "data")
    public void aTests(String InputFolderWithJsonFiles, String Res) throws Exception {
        //String JSONInput = Utils.FileToString(JsonInputFile);

        File f = new File(InputFolderWithJsonFiles);
        // Populates the array with names of files and directories
        String[] pathnames = f.list();

        // For each pathname in the pathnames array
        for (String pathname : pathnames) {

            File a = new File(InputFolderWithJsonFiles+"/"+pathname);
            System.out.println(a.getAbsoluteFile());
            if(!a.isDirectory() && pathname.endsWith(".json")){
                System.out.println("Processing: "+a.getAbsolutePath());
                StringValue d = command.action(a.getAbsolutePath(),true,a.getAbsolutePath());
            }
        }





    }
}

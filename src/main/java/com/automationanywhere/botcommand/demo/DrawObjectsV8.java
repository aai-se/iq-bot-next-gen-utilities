package com.automationanywhere.botcommand.demo;


import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonGraphicsOperations;
import com.automationanywhere.utils.FileProcessor;
import org.testng.internal.Utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand
@CommandPkg(label="Draw Objects", name="Draw Objects - V8", description="Draw Objects",  icon="",
        node_label="Draw Objects" ,
        return_type= STRING, return_label="Status", return_required=false)
public class DrawObjectsV8 {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    //private static final Logger logger = LogManager.getLogger(DetectObjects.class);

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public StringValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Json Input", default_value_type = STRING,  default_value = "Default") @NotEmpty String JsonInput,
            @Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Input Document (png file)", default_value_type = DataType.FILE) @NotEmpty String InputFilePath,
            @Idx(index = "3", type = AttributeType.FILE) @Pkg(label = "Output Document (png file)", default_value_type = DataType.FILE) @NotEmpty String OutputFilePath

    ) throws Exception {

        if("".equals(JsonInput)) {
            throw new BotCommandException(MESSAGES.getString("emptyInputString", "BoundingBoxHexColor"));
        }

        File InputFile = new File(InputFilePath);



        if(! InputFile.exists()) {
            throw new BotCommandException(MESSAGES.getString("inputFileMissing",InputFilePath ));
        }

        // Loading File Processor for multiple page pdfs
        FileProcessor fp = new FileProcessor(InputFilePath);
        //ArrayList<String> AllImages = fp.GetFileAsImages();

        if(fp.isFileAPdf()){
            // Not Supported Yet
            fp.Cleanup();
        }else{

            Image image = ImageIO.read(InputFile);
            BufferedImage BufferedInputFile = (BufferedImage) image;
            BufferedImage BufferedOutputFile = CommonGraphicsOperations.drawObjectsV8(BufferedInputFile,JsonInput);

            //int height = BufferedOutputFile.getHeight();
            //System.out.println("Height:"+height);
            //System.out.println("output:"+OutputFilePath);
            if(BufferedOutputFile != null){
                ImageIO.write(BufferedOutputFile, "png", new File(OutputFilePath));
            }



        }

        return new StringValue("");

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}






package com.automationanywhere.botcommand.demo;


import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonGraphicsOperations;
import com.automationanywhere.utils.FileProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.LIST;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand
@CommandPkg(label="Draw Objects - Form Recognizer", name="Draw Objects - Form Recognizer", description="Draw Objects - Form Recognizer",  icon="",
        node_label="Draw Objects - Form Recognizer" ,
        return_type= STRING, return_label="Status", return_required=true)
public class DrawObjectsMSFormRecognizer {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    private static final Logger logger = LogManager.getLogger(DrawObjectsMSFormRecognizer.class);

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public StringValue action(
            @Idx(index = "1", type = LIST) @Pkg(label = "List of Dictionaries") @NotEmpty List<Value> DictionaryList,
            @Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Input Document (PDF or Image)", default_value_type = DataType.FILE) @NotEmpty String InputFilePath,
            @Idx(index = "3", type = AttributeType.FILE) @Pkg(label = "Output Document (PDF or Image)", default_value_type = DataType.FILE) @NotEmpty String OutputFilePath,
            @Idx(index = "4", type = TEXT) @Pkg(label = "Bounding Box Hex Color", default_value_type = STRING, default_value = "#f55d42",description = "ex: #f55d42")  String BoundingBoxHexColor,
            @Idx(index = "5", type = AttributeType.NUMBER) @Pkg(label = "Bounding Box Thickness", default_value_type = DataType.NUMBER, default_value = "4",description = "ex: 4")  Number LineThickness,
            @Idx(index = "6", type = TEXT) @Pkg(label = "Label Box Color", default_value_type = STRING, default_value = "#f0f0f0",description = "ex: #f0f0f0")  String LabelBoxHexColor,
            @Idx(index = "7", type = TEXT) @Pkg(label = "Label Box Text Color", default_value_type = STRING, default_value = "#5ecaeb",description = "ex: #5ecaeb")  String LabelTextColor
    ) throws Exception {

        File InputFile = new File(InputFilePath);

        logger.info("[MS Form Recognizer Draw] - DictionaryList: "+DictionaryList.size()) ;

        if(! InputFile.exists()) {
            throw new BotCommandException(MESSAGES.getString("inputFileMissing",InputFilePath ));
        }


        // Loading File Processor for multiple page pdfs
        FileProcessor fp = new FileProcessor(InputFilePath);
        //ArrayList<String> AllImages = fp.GetFileAsImages();

        if(fp.isFileAPdf()){
            // Not Supported Yet
            fp.Cleanup();
        }else{

            Image image = ImageIO.read(InputFile);
            BufferedImage BufferedInputFile = (BufferedImage) image;
            //String LabelBoxHexColor = "#f0f0f0";
            // LabelTextColor = "#5ecaeb";

            BufferedImage BufferedOutputFile = CommonGraphicsOperations.drawDetectedObjectsMSRecv1(BufferedInputFile,DictionaryList,BoundingBoxHexColor,LineThickness, LabelBoxHexColor, LabelTextColor);

            //int height = BufferedOutputFile.getHeight();
            //System.out.println("Height:"+height);
           // System.out.println("output:"+OutputFilePath);
            ImageIO.write(BufferedOutputFile, "png", new File(OutputFilePath));

        }

        return new StringValue("");

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}






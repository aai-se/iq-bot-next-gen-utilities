package com.automationanywhere.botcommand.demo;


import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonConversionOperations;
import com.automationanywhere.cvlogics.CommonGraphicsOperations;
import com.automationanywhere.utils.FileProcessor;
import com.automationanywhere.utils.Utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand
@CommandPkg(label="Convert to CSV", name="Convert to CSV", description="Convert to CSV",  icon="",
        node_label="Convert to CSV" ,
        return_type= STRING, return_label="Status", return_required=false)
public class JSONToCSV {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    //private static final Logger logger = LogManager.getLogger(DetectObjects.class);

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public StringValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Json Input", default_value_type = STRING,  default_value = "C:/iqbot/V8/output/my_file.json") @NotEmpty String JsonInput,
            @Idx(index = "2", type = AttributeType.BOOLEAN) @Pkg(label = "Input is a JSON File", default_value_type = DataType.BOOLEAN,  default_value = "True") @NotEmpty Boolean InputIsJSONFile,
            @Idx(index = "3", type = AttributeType.FILE) @Pkg(label = "CSV Output File", default_value_type = DataType.FILE,default_value = "C:/iqbot/V8/output/my_file") @NotEmpty String OutputFilePathRoot
            //@Idx(index = "3", type = AttributeType.FILE) @Pkg(label = "CSV Output Document (for Table data)", default_value_type = DataType.FILE) @NotEmpty String OutputFilePathTable

    ) throws Exception {

        if("".equals(JsonInput)) {
            throw new BotCommandException(MESSAGES.getString("emptyInputString"));
        }
        String JSONINPUT = JsonInput;
        if(InputIsJSONFile){
            JSONINPUT = Utils.FileToString(JsonInput);
        }

        //System.out.println(JSONINPUT);
        boolean Status = CommonConversionOperations.ConvertToCsv(OutputFilePathRoot,JSONINPUT);
        return new StringValue("");

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}






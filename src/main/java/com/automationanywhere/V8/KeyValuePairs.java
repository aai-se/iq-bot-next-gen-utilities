package com.automationanywhere.V8;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class KeyValuePairs {

    private JSONArray AllKeyPairValues;
    public ArrayList<KeyValuePair> AllPairsList = new ArrayList<KeyValuePair>();

    public KeyValuePairs(JSONArray AllPairs){
        this.AllKeyPairValues = AllPairs;
        //System.out.println("DEBUG:"+AllPairs.toString());

        if(this.AllKeyPairValues.size()>0){
            for(int i =0;i<this.AllKeyPairValues.size();i++) {
                KeyValuePair myPair = new KeyValuePair((JSONObject) this.AllKeyPairValues.get(i));
                this.AllPairsList.add(myPair);
            }
        }

    }

    public ArrayList<String> getCSV(ArrayList<KeyValuePair> AllPairs){
        ArrayList<String> AllBlockInfo = new ArrayList<String>();

        for(int i=0;i<AllPairs.size();i++){
            KeyValuePair aBlock = AllPairs.get(i);
            String BlockInfo = aBlock.ToCsv(",");
            AllBlockInfo.add(BlockInfo);
            //System.out.println(BlockInfo);
        }
        return AllBlockInfo;
    }

    public static BufferedImage drawPairs(BufferedImage img, ArrayList<KeyValuePair> AllPairs){

        String HCBoundingBox = "#4418d6";

        String HCLabelBoxBackgroundColor = "#f0f0f0";
        String HCLabelBoxFontColor = HCBoundingBox;

        Number LabelFontThickness = 2;

        BufferedImage imgResult = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        Graphics2D g = imgResult.createGraphics();
        g.drawImage(img, 0, 0, null);

        for(int i=0;i<AllPairs.size();i++){
            KeyValuePair aBlock = AllPairs.get(i);

            g.setColor(V8Utils.hex2Rgb(HCBoundingBox));
            g.drawRect(aBlock.x, aBlock.y, aBlock.width+2, aBlock.height+2);

            // Calculate the Label Box length
            g.setColor(V8Utils.hex2Rgb(HCLabelBoxBackgroundColor));
            //String Label = aBlock.BlockType+" | "+aBlock.ODConfidence+"%"+" | "+aBlock.BlockKeyType;
            String Label = aBlock.DetectionConfidence+"% - "+aBlock.BlockKeyType;
            int LabelRectLength = g.getFontMetrics().stringWidth(Label)+8;

            // Fill the label Box
            //g.fillRect(aBlock.x-LabelRectLength, aBlock.y, LabelRectLength, aBlock.height+2);
            g.fillRect(aBlock.x, aBlock.y-10, LabelRectLength, 15);
            // Write Data in Label Box
            float thickness = LabelFontThickness.floatValue();
            Stroke stroke = new BasicStroke(thickness);

            g.setStroke(stroke);
            g.setColor(V8Utils.hex2Rgb(HCLabelBoxFontColor));
            g.drawRect(aBlock.x, aBlock.y-12, LabelRectLength, 15);


            g.setFont(new Font(g.getFont().getName(), Font.PLAIN, 10));
            g.drawString(Label, aBlock.x+2, aBlock.y);
        }
        return imgResult;
    }
}

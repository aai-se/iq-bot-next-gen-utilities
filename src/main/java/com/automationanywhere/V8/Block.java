package com.automationanywhere.V8;

import org.json.simple.JSONObject;

import java.text.DecimalFormat;

public class Block {

    public String BlockType;
    public String InfoBlockType;
    public String BlockID;
    public String BlockText;
    //public String BlockKeyType;
    //public String BlockValue;
    public Double DetectionConfidence;
    public Double OCRConfidence;
    public Double ExtractionScore;

    public int x;
    public int y;
    public int width;
    public int height;

    public Block(JSONObject aBlock){

        this.BlockID = (String) aBlock.get("id");
        this.BlockType = (String) aBlock.get("blockType");
        this.InfoBlockType = (String) aBlock.get("infoBlockType");

        this.DetectionConfidence = V8Utils.NormalizeScore((Double) aBlock.get("detectionConfidence"));
        this.BlockText = (String) aBlock.get("text");
        this.OCRConfidence = V8Utils.NormalizeScore((Double) aBlock.get("ocrConfidence"));
        this.ExtractionScore = V8Utils.NormalizeScore((Double) aBlock.get("extractionScore"));
        //this.BlockKeyType = (String) aBlock.get("keyType");
        //this.BlockValue = (String) aBlock.get("value");

        JSONObject BlockGeometry = (JSONObject)aBlock.get("geometry");

        //this.ODConfidence= V8Utils.NormalizeODConfidenceScore(aBlock);
        //this.OCRConfidence = V8Utils.NormalizeOCRConfidenceScore(aBlock);

        //System.out.println("OCR Score: "+this.OCRConfidence);
        //System.out.println("OD Score: "+this.ODConfidence);

        Long X1 = (Long) BlockGeometry.get("x1");
        Long Y1 = (Long) BlockGeometry.get("y1");
        Long X2 = (Long) BlockGeometry.get("x2");
        Long Y2 = (Long) BlockGeometry.get("y2");

        this.x = X1.intValue();
        this.y = Y1.intValue();
        this.height= X2.intValue();
        this.width = Y2.intValue();

        //this.width= (int)(X2-this.x);
        //this.height = (int)(Y2-this.y);
    }

    public String ToCsv(String Separator){
        if(BlockText.contains(",")){
            BlockText = "\""+BlockText+"\"";
        }

        //BlockType+Separator+BlockKeyType+Separator+BlockID+Separator+BlockText+Separator+BlockKey+Separator+BlockValue+Separator+DetectionConfidence+Separator+OCRConfidence+Separator+ExtractionScore+Separator+x+Separator+y+Separator+width+Separator+height;
        String CsvString = BlockType+Separator+InfoBlockType+Separator+BlockID+Separator+BlockText+Separator+"NA"+Separator+"NA"+Separator+DetectionConfidence+Separator+OCRConfidence+Separator+ExtractionScore+Separator+x+Separator+y+Separator+width+Separator+height;
        return CsvString;
    }

}

package com.automationanywhere.V8;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;

public class TableRows {


    public ArrayList<TableRow> AllRows = new ArrayList<TableRow>();

    public TableRows(JSONArray allJsonObjects){

        for(int i=0;i<allJsonObjects.size();i++){

            JSONArray aRow = (JSONArray) allJsonObjects.get(i);
            TableRow row = new TableRow(i+1,aRow);
            this.AllRows.add(row);
        }
    }

    public ArrayList<String> getCSV(String Separator){
        ArrayList<String> AllInfo = new ArrayList<String>();
        for(TableRow r : this.AllRows){
            AllInfo.addAll(r.getCSV(Separator));
        }
        return AllInfo;
    }

}

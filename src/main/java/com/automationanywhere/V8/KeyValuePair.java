package com.automationanywhere.V8;

import org.json.simple.JSONObject;

public class KeyValuePair {

    public String BlockType;
    public String BlockID;
    public String BlockText;
    public String BlockKey;
    public String BlockKeyType;
    public String BlockValue;

    public Double DetectionConfidence;
    public Double OCRConfidence;
    public Double ExtractionScore;

    //public String ODConfidence;
    //public String OCRConfidence;
    public int x;
    public int y;
    public int width;
    public int height;

    public KeyValuePair(JSONObject aBlock){

        this.BlockID = (String) aBlock.get("id");
        this.BlockType = (String) aBlock.get("blockType");
        this.BlockKeyType = (String) aBlock.get("keyType");

        this.DetectionConfidence = V8Utils.NormalizeScore((Double)aBlock.get("detectionConfidence"));
        this.BlockText = (String) aBlock.get("text");
        this.BlockKey = (String) aBlock.get("key");
        this.BlockValue = (String) aBlock.get("value");
        this.OCRConfidence = V8Utils.NormalizeScore((Double) aBlock.get("ocrConfidence"));
        this.ExtractionScore = V8Utils.NormalizeScore((Double) aBlock.get("extractionScore"));




        JSONObject BlockGeometry = (JSONObject)aBlock.get("geometry");

        //this.ODConfidence= V8Utils.NormalizeODConfidenceScore(aBlock);
        //this.OCRConfidence = V8Utils.NormalizeOCRConfidenceScore(aBlock);

        //System.out.println("OCR Score: "+this.OCRConfidence);
        //System.out.println("OD Score: "+this.ODConfidence);

        Long X1 =-1L;
        Long Y1 =-1L;
        Long X2 =-1L;
        Long Y2 =-1L;

        if(BlockGeometry!=null) {
            //System.out.println("Geometry: " + aBlock.toString());
            X1 = (Long) BlockGeometry.get("x1");
            Y1 = (Long) BlockGeometry.get("y1");
            X2 = (Long) BlockGeometry.get("x2");
            Y2 = (Long) BlockGeometry.get("y2");
        }
        this.x = X1.intValue();
        this.y = Y1.intValue();
        this.height= X2.intValue();
        this.width = Y2.intValue();
        //this.width= (int)(X2-this.x);
        //this.height = (int)(Y2-this.y);
    }

    public String ToCsv(String Separator){

        BlockText = ProtectSeparatorIfNeeded(Separator,this.BlockText);
        BlockKey = ProtectSeparatorIfNeeded(Separator,this.BlockKey);
        BlockValue = ProtectSeparatorIfNeeded(Separator,this.BlockValue);

        String CsvString = BlockType+Separator+BlockKeyType+Separator+BlockID+Separator+BlockText+Separator+BlockKey+Separator+BlockValue+Separator+DetectionConfidence+Separator+OCRConfidence+Separator+ExtractionScore+Separator+x+Separator+y+Separator+width+Separator+height;
        return CsvString;
    }

    public String ProtectSeparatorIfNeeded(String Separator, String Text){
        if(Text.contains(Separator)){
            Text = "\""+Text+"\"";
        }
        return Text;
    }
}

package com.automationanywhere.V8;

import com.automationanywhere.botcore.api.dto.Row;
import org.json.simple.JSONObject;

import java.awt.*;
import java.awt.image.BufferedImage;

public class RowElement {

    public String ID;
    public String Text;
    public Double OCRConfidence;
    public Double ExtractionScore;
    public String x;
    public String y;
    public String height;
    public String width;
    public int RowNumber;
    public int ElementNumber;

    public RowElement(int RowNumber, int ElementNumber,JSONObject anElement){

        this.RowNumber = RowNumber;
        this.ElementNumber = ElementNumber;
        this.ID = (String) anElement.get("id");
        this.Text = (String) anElement.get("text");
        JSONObject BoundingBox = (JSONObject) anElement.get("geometry");

        this.OCRConfidence = V8Utils.NormalizeScore((Double) anElement.get("ocrConfidence"));
        this.ExtractionScore = V8Utils.NormalizeScore((Double) anElement.get("extractionScore"));
        // The Rest can potentially either not exist or be null..

        this.x="-1";
        this.y="-1";
        this.height = "-1";
        this.width ="-1";

        if(BoundingBox != null){
            Long yL = (Long) BoundingBox.get("x1");
            Long xL = (Long) BoundingBox.get("y1");
            Long heightL = (Long) BoundingBox.get("x2");
            Long widthL = (Long) BoundingBox.get("y2");
            this.x = xL.toString();
            this.y = yL.toString();
            this.height = heightL.toString();
            this.width = widthL.toString();
        }

        /*
        if(anElement.get("ocrConfidence") != null){
            Long ocrConfL = (Long) anElement.get("ocrConfidence");
            this.OCRConfidence = ocrConfL.toString();
        }

        if(anElement.get("score") != null){
            Long scoreL = (Long) anElement.get("score");
            this.Score = scoreL.toString();
        }
*/

    }

    public String getCSV(String Separator){
        String Output = "";
        if(this.Text.contains(",")){
            this.Text = "\""+this.Text+"\"";
        }
        Output = "TableRowElement"+Separator+this.RowNumber+Separator+this.ElementNumber+Separator+"NA"+Separator+this.Text+Separator+"NA"+Separator+this.OCRConfidence+Separator+this.ExtractionScore+Separator+this.x+Separator+this.y+Separator+this.height+Separator+this.width;
        return Output;
    }

    public BufferedImage drawElement(BufferedImage img, Color HexColor){


        BufferedImage imgResult = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        Graphics2D g = imgResult.createGraphics();
        g.drawImage(img, 0, 0, null);

            if(!this.Text.equals("")){
                System.out.println("Row Element Found: "+this.Text+"|"+this.OCRConfidence);
                String HCBoundingBox = "#0eed89";
                g.drawImage(imgResult, 0, 0, null);

                g.setColor(HexColor);
                int ix = Integer.valueOf(this.x);
                int iy = Integer.valueOf(this.y);
                int iheight = Integer.valueOf(this.height);
                int iwidth = Integer.valueOf(this.width);

                // "height": 18,
                //         "left": 82, / X
                //         "top": 1095, / Y
                //         "width": 352

                //System.out.println("DEBUG:"+ix+":"+iy+":"+iwidth+":"+iheight);
                //g.drawRect(82,1095 ,352, 18 );
                g.drawRect(ix,iy ,iwidth, iheight );
            }



        return imgResult;
    }
}

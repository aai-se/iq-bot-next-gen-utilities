package com.automationanywhere.V8;

import org.json.simple.JSONObject;

import java.awt.*;
import java.text.DecimalFormat;

public class V8Utils {

    public static String NormalizeODConfidenceScore(JSONObject aJsonObject){

        String ODConfidence;
        try{
            ODConfidence = (String) aJsonObject.get("confidence");
        }catch(ClassCastException e){
            try{
                Long TempConf = (Long) aJsonObject.get("confidence");
                ODConfidence = TempConf.toString();
            }catch(ClassCastException f){
                Double TempConf = (Double) aJsonObject.get("confidence");
                ODConfidence = TempConf.toString();
            }

        }
       // System.out.println("Actual OD Conf:"+ODConfidence);
        double d = Double.valueOf(ODConfidence);

        if(d <= 1){
            d = d*100;
        }

        DecimalFormat df = new DecimalFormat("##.##");
        String OCRConfidenceS = df.format(d);
        return OCRConfidenceS;
    }

    public static Double NormalizeScore(Double aScore){
        return aScore*100;
    }

    public static String NormalizeOCRConfidenceScore(JSONObject aJsonObject){
        double OCRConfidence=0;
        if(aJsonObject.get("ocr_confidence") != null){
            try{
                OCRConfidence = (Double) aJsonObject.get("ocr_confidence");
            }catch(ClassCastException e){
                Long TempConf = (Long) aJsonObject.get("ocr_confidence");
                OCRConfidence = TempConf.doubleValue();
            }
        }else{
            OCRConfidence=-1.0;
        }

        DecimalFormat df = new DecimalFormat("##.##");

        String OCRConfidenceS = df.format(OCRConfidence);

        double OCRConfidenceRetrieved = Double.valueOf(OCRConfidenceS);

        //System.out.println("Actual OCR Conf:"+OCRConfidenceRetrieved);

        if(OCRConfidenceRetrieved <= 1 && OCRConfidenceRetrieved > -1){
            OCRConfidenceRetrieved = OCRConfidenceRetrieved*100;
        }



        return df.format(OCRConfidenceRetrieved);

    }

    public static Color brighten(Color color, double fraction) {

        int red = (int) Math.round(Math.min(255, color.getRed() + 255 * fraction));
        int green = (int) Math.round(Math.min(255, color.getGreen() + 255 * fraction));
        int blue = (int) Math.round(Math.min(255, color.getBlue() + 255 * fraction));

        int alpha = color.getAlpha();

        return new Color(red, green, blue, alpha);

    }

    public static Color getLighterColor(Color color, int cycles){
        for(int i=0;i<cycles;i++){
            color = color.brighter();//color.brighter();//color.brighter();color.brighter();
        }
        return color;
    }

    public static String NormalizeScore(JSONObject aJsonObject, String TagName){
        double OCRConfidence=0;
        if(aJsonObject.get(TagName) != null){
            try{
                OCRConfidence = (Double) aJsonObject.get(TagName);
            }catch(ClassCastException e){
                Long TempConf = (Long) aJsonObject.get(TagName);
                OCRConfidence = TempConf.doubleValue();
            }
        }else{
            OCRConfidence=-1.0;
        }

        DecimalFormat df = new DecimalFormat("##.##");

        String OCRConfidenceS = df.format(OCRConfidence);

        double OCRConfidenceRetrieved = Double.valueOf(OCRConfidenceS);

        //System.out.println("Actual OCR Conf:"+OCRConfidenceRetrieved);

        if(OCRConfidenceRetrieved <= 1 && OCRConfidenceRetrieved > -1){
            OCRConfidenceRetrieved = OCRConfidenceRetrieved*100;
        }
        return df.format(OCRConfidenceRetrieved);

    }

    public static Color hex2Rgb(String colorStr) {
        return new Color(
                Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
                Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
                Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) );
    }

}

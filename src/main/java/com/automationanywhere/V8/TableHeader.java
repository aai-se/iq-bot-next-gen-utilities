package com.automationanywhere.V8;

public class TableHeader {

    public String Type;
    public String ID;
    public String Text;
    public Double DetectionConfidence;
    public Double OCRConfidence;
    public Double ExtractionScore;
    public int Position;
    public Long x1;
    public Long y1;
    public Long Height;
    public Long Width;


    public TableHeader(int HeaderPosition,String Type, String ID, String Text, Double DetectionConfidence, Double OCRConfidence, Double ExtractionScore,Long X1,Long Y1,Long HEIGHT,Long WIDTH){

        this.ID = ID;
        this.Text = Text;
        this.Position=HeaderPosition;
        // Geometry missing
        this.Type = Type;
        this.DetectionConfidence = DetectionConfidence;
        this.OCRConfidence=OCRConfidence;
        this.ExtractionScore = ExtractionScore;
        this.x1 = X1;
        this.y1=Y1;
        this.Height=HEIGHT;
        this.Width=WIDTH;

        this.DetectionConfidence = V8Utils.NormalizeScore(DetectionConfidence);
        this.OCRConfidence = V8Utils.NormalizeScore(OCRConfidence);
        this.ExtractionScore = V8Utils.NormalizeScore(ExtractionScore);

    }

    public String getCSV(String Separator){
        String Output = "";
        if(this.Text.contains(",")){
            this.Text = "\""+this.Text+"\"";
        }
        Output = "TableHeader"+Separator+"0"+Separator+this.Position+Separator+this.Type+Separator+this.Text+Separator+this.DetectionConfidence+Separator+this.OCRConfidence+Separator+this.ExtractionScore+Separator+this.x1+Separator+this.y1+Separator+this.Height+Separator+this.Width;
        return Output;
    }

}

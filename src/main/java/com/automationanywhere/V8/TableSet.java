package com.automationanywhere.V8;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class TableSet {

    public String ID;
    public String TableName;
    public TableHeaders allHeaders;
    public TableRows allRows;


    public TableSet(JSONObject anObject){

        this.ID = (String) anObject.get("id");
        this.TableName = (String) anObject.get("TableName");

        JSONArray AllHeadersJson = (JSONArray) anObject.get("headers");

        this.allHeaders = new TableHeaders(AllHeadersJson);

        JSONArray AllRowsJson = (JSONArray) anObject.get("rows");
        this.allRows = new TableRows(AllRowsJson);
    }

    public ArrayList<String> getCSV(String Separator){
        ArrayList<String> AllTableItems = new ArrayList<String>();

        AllTableItems.addAll(this.allHeaders.getCSV(Separator));
        AllTableItems.addAll(this.allRows.getCSV(Separator));
        return AllTableItems;
    }
    public BufferedImage drawTable(BufferedImage img){

        BufferedImage imgResult = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        Graphics2D g = imgResult.createGraphics();
        g.drawImage(img, 0, 0, null);


        System.out.println("Number of Headers:"+this.allHeaders.AllHeaders.size());
        System.out.println("Number of Rows:"+this.allRows.AllRows.size());

        for(TableHeader header : this.allHeaders.AllHeaders){
            System.out.println("Headers: "+header.Type+"|"+header.DetectionConfidence);
        }

        Color ColorOdd = V8Utils.hex2Rgb("#03ff41");
        Color ColorEven = V8Utils.hex2Rgb("#008521");
        Color CurrentColor = ColorOdd;
        int idx = 1;
        for(TableRow row : this.allRows.AllRows){
            if(!row.isRowEmpty()){
                if(CurrentColor.equals(ColorOdd)){
                    CurrentColor = ColorEven;
                }else if(CurrentColor.equals(ColorEven)){
                    CurrentColor = ColorOdd;
                }

                System.out.println("Color:"+CurrentColor.toString());
                imgResult = row.drawRow(imgResult,CurrentColor);
            }


            idx=idx+1;
        }

        return imgResult;
    }

}

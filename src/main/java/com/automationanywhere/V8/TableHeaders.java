package com.automationanywhere.V8;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;

public class TableHeaders {

    public ArrayList<TableHeader> AllHeaders = new ArrayList<TableHeader>();

    public TableHeaders(JSONArray allJsonObjects){

        for(int i=0;i<allJsonObjects.size();i++){

            JSONObject anObject = (JSONObject) allJsonObjects.get(i);

            String ID = (String) anObject.get("id");
            String Text = (String) anObject.get("text");
            String Type = (String) anObject.get("tableHeaderType");

            //String tagConfidence= V8Utils.NormalizeScore(anObject,"tagConfidence");
            //String ocrConfidence = V8Utils.NormalizeScore(anObject,"ocrConfidence");
            //String Score = V8Utils.NormalizeScore(anObject,"score");
            Double detectionConfidence = (Double) anObject.get("detectionConfidence");
            Double ocrConfidence = (Double) anObject.get("ocrConfidence");
            Double extractionScore = (Double) anObject.get("extractionScore");

            JSONObject BlockGeometry = (JSONObject)anObject.get("geometry");

            Long X1 = (Long) BlockGeometry.get("x1");
            Long Y1 = (Long) BlockGeometry.get("y1");
            Long HEIGHT = (Long) BlockGeometry.get("x2");
            Long WIDTH = (Long) BlockGeometry.get("y2");

            TableHeader header = new TableHeader(i,Type,ID,Text,detectionConfidence,ocrConfidence,extractionScore,X1,Y1,HEIGHT,WIDTH);

            this.AllHeaders.add(header);

        }

    }

    public ArrayList<String> getCSV(String Separator){
        ArrayList<String> AllInfo = new ArrayList<String>();
        for(TableHeader r : this.AllHeaders){
            AllInfo.add(r.getCSV(Separator));
        }
        return AllInfo;
    }


}

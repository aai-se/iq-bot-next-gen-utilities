package com.automationanywhere.V8;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class TableRow {

    public ArrayList<RowElement> AllElementsInRow = new ArrayList<RowElement>();

    public TableRow(int RowNumber,JSONArray myObj){

        for(int i = 0;i<myObj.size();i++){
            JSONObject anElement = (JSONObject) myObj.get(i);
            RowElement element = new RowElement(RowNumber,i,anElement);
            this.AllElementsInRow.add(element);
        }
    }

    public ArrayList<String> getCSV(String Separator){
        ArrayList<String> AllEntries = new ArrayList<String>();
        for(RowElement el:this.AllElementsInRow){
            String s = el.getCSV(Separator);
            AllEntries.add(s);
        }
        return AllEntries;
    }

    public BufferedImage drawRow(BufferedImage img, Color HexColor){
        for(RowElement el : this.AllElementsInRow){
            img = el.drawElement(img, HexColor);
        }
        return img;
    }

    public boolean isRowEmpty(){
        for(RowElement el : AllElementsInRow){
            if(!el.Text.equals("")){
                return false;
            }
        }
        return true;
    }
}

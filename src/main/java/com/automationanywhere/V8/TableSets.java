package com.automationanywhere.V8;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class TableSets {

    private JSONArray AllTableSets;
    public ArrayList<TableSet> AllTables = new ArrayList<TableSet>();

    public TableSets(JSONArray AllObjects){
        this.AllTableSets = AllObjects;

        for(int i =0;i<this.AllTableSets.size();i++) {
            JSONObject anObject = (JSONObject) this.AllTableSets.get(i);
            TableSet myObj = new TableSet(anObject);
            this.AllTables.add(myObj);
        }
        //System.out.println("DEBUG:"+this.AllTables.size());
    }

    public ArrayList<ArrayList<String>> getCSVs(String Separator){
        ArrayList<ArrayList<String>> AllTables = new ArrayList<ArrayList<String>>();
        ArrayList<String> aTableData = new ArrayList<String>();
        String Headers = "TableHeader,RowIndex,ElementIndexInRow,Type,Text,DetectionConfidence,OCRConfidence,ExtractionScore,x,y,width,height";
        for(TableSet t:this.AllTables){
            //System.out.println("Processing a Table");
            aTableData.add(Headers);
            aTableData.addAll(t.getCSV(Separator));
            AllTables.add(aTableData);
        }

        return AllTables;
    }

    public static BufferedImage drawTables(BufferedImage img, ArrayList<TableSet> AllTables){

        for(TableSet table : AllTables){
            img = table.drawTable(img);
        }

        return img;
    }

}

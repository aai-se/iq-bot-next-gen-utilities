package com.automationanywhere.V8;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Blocks {
    private JSONArray AllBlocks;

    public ArrayList<Block> AllBlocksList = new ArrayList<Block>();

    public Blocks(JSONArray AllBlocks){
        this.AllBlocks = AllBlocks;
        for(int i =0;i<this.AllBlocks.size();i++) {
            Block myBlock = new Block((JSONObject) this.AllBlocks.get(i));
            this.AllBlocksList.add(myBlock);
        }
    }

    public ArrayList<String> getCSV(ArrayList<Block> AllBlocks){
        ArrayList<String> AllBlockInfo = new ArrayList<String>();

        for(int i=0;i<AllBlocks.size();i++){
            Block aBlock = AllBlocks.get(i);
            String BlockInfo = aBlock.ToCsv(",");
            AllBlockInfo.add(BlockInfo);
            //System.out.println(BlockInfo);
        }
        return AllBlockInfo;
    }


    public static BufferedImage drawBlocks(BufferedImage img, ArrayList<Block> AllBlocks){

        // Block Types: no_object, tbl,tbl_header,tbl_hdr_elem, column, , handwriting, , graphic, info_block,

        String HCBoundingBox_NOOBJECT = "#f00cc2";

        String HCBoundingBox_TABLE = "#fc0f03";
        String HCBoundingBox_TBLHEADER = "#fc7703";
        String HCBoundingBox_TABLEHEADER_ELEMENT = "#fca903";
        String HCBoundingBox_COLUMN = "#fce703";

        String HCBoundingBox_HANDWRITING = "#fc03f4";

        String HCBoundingBox_GRAPHIC = "#18b6d6";
        String HCBoundingBox_INFOBLOCK = "#1867d6";


        String HCLabelBoxBackgroundColor = "#f0f0f0";
        String HCLabelBoxFontColor = "#5ecaeb";
        Number LabelFontThickness = 2;
        Boolean ShowNoObject = true;


        BufferedImage imgResult = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        Graphics2D g = imgResult.createGraphics();
        g.drawImage(img, 0, 0, null);

        for(int i=0;i<AllBlocks.size();i++){
            Block aBlock = AllBlocks.get(i);

            // Draw Bounding Box
            String HCBoundingBox = HCBoundingBox_NOOBJECT;
            String Label = "OD="+aBlock.DetectionConfidence+"%" + " | OCR="+aBlock.OCRConfidence+"%"+" - "+aBlock.BlockType;
            boolean IsObjectaNoObject = false;
            if(aBlock.BlockType.equals("no_object")){IsObjectaNoObject = true;HCBoundingBox = HCBoundingBox_NOOBJECT;Label = "";}
            if(aBlock.BlockType.equals("tbl")){HCBoundingBox = HCBoundingBox_TABLE;}
            if(aBlock.BlockType.equals("tbl_header")){HCBoundingBox = HCBoundingBox_TBLHEADER;Label = "OD="+aBlock.DetectionConfidence+"%" + " | OCR="+aBlock.OCRConfidence+"%"+" | all_headers";}
            if(aBlock.BlockType.equals("tbl_hdr_elem")){HCBoundingBox = HCBoundingBox_TABLEHEADER_ELEMENT;Label = "OD="+aBlock.DetectionConfidence+"%" + " | OCR="+aBlock.OCRConfidence+"%";}
            if(aBlock.BlockType.equals("column")){HCBoundingBox = HCBoundingBox_COLUMN;Label = "OD="+aBlock.DetectionConfidence+"%" + " | OCR="+aBlock.OCRConfidence+"%";}
            if(aBlock.BlockType.equals("handwriting")){HCBoundingBox = HCBoundingBox_HANDWRITING;}
            if(aBlock.BlockType.equals("graphic")){HCBoundingBox = HCBoundingBox_GRAPHIC;}
            if(aBlock.BlockType.equals("info_block")){HCBoundingBox = HCBoundingBox_INFOBLOCK;}

            if(ShowNoObject || !aBlock.BlockType.equals("no_object")){
                g.setColor(V8Utils.hex2Rgb(HCBoundingBox));
                g.drawRect(aBlock.x, aBlock.y, aBlock.width+2, aBlock.height+2);

                // Dont draw Label box if a "no object"
                if(!IsObjectaNoObject){
                    // Calculate the Label Box length
                    g.setColor(V8Utils.hex2Rgb(HCLabelBoxBackgroundColor));
                    //String Label = aBlock.BlockType+" | "+aBlock.ODConfidence+"%"+" | "+aBlock.BlockKeyType;
                    //String Label = "OD="+aBlock.ODConfidence+"%" + " | OCR="+aBlock.OCRConfidence+"%"+" - "+aBlock.BlockType;
                    int LabelRectLength = g.getFontMetrics().stringWidth(Label)+8;

                    // Fill the label Box
                    //g.fillRect(aBlock.x-LabelRectLength, aBlock.y, LabelRectLength, aBlock.height+2);
                    g.fillRect(aBlock.x, aBlock.y-10, LabelRectLength, 15);
                    // Write Data in Label Box
                    float thickness = LabelFontThickness.floatValue();
                    Stroke stroke = new BasicStroke(thickness);

                    g.setStroke(stroke);
                    g.setColor(V8Utils.hex2Rgb(HCBoundingBox));
                    g.drawRect(aBlock.x, aBlock.y-12, LabelRectLength, 15);
                    g.setFont(new Font(g.getFont().getName(), Font.PLAIN, 10));
                    g.drawString(Label, aBlock.x+2, aBlock.y);
                }

            }

        }
        return imgResult;
    }


}

package com.automationanywhere.cvlogics;

import com.automationanywhere.V8.*;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcore.api.dto.KeyValueItem;

import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.awt.*;
import java.awt.image.BufferedImage;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class CommonGraphicsOperations {

        public static String FileToString(String filePath)
        {
            StringBuilder contentBuilder = new StringBuilder();

            try (Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8))
            {
                stream.forEach(s -> contentBuilder.append(s).append("\n"));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return contentBuilder.toString();
        }

        public static BufferedImage drawDetectedObjectsMSRecv1(BufferedImage img, List<Value> AllDictionaries, String BoundingBoxHexColor,Number LineThickness,String LabelBoxHexColor, String LabelTextColor){

            BufferedImage result = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
            //Graphics g = result.getGraphics();
            Graphics2D g = result.createGraphics();
            g.drawImage(img, 0, 0, null);

            //System.out.println("DEBUG SIZE:"+AllDictionaries.size());
            for(int i=0;i<AllDictionaries.size();i++){
                DictionaryValue val = (DictionaryValue) AllDictionaries.get(i);

                Map<String,Value> theMap = val.get();

                String KeyName = theMap.get("keyname").toString();
                //System.out.println("DEBUG in:"+KeyName);

                String TheText = theMap.get("text").toString();
                String X0 = theMap.get("x").toString();
                String Y0 = theMap.get("y").toString();
                String WIDTH = theMap.get("width").toString();
                String HEIGHT = theMap.get("height").toString();
                String SCORE = theMap.get("score").toString();
                String PAGE = theMap.get("page").toString();
                String TYPE = theMap.get("type").toString();

                Double x0 = Double.parseDouble(X0);
                Double y0 = Double.parseDouble(Y0);
                Double Width = Double.parseDouble(WIDTH);
                Double Height = Double.parseDouble(HEIGHT);
                String Label =  TYPE+" | "+SCORE+"%"+" | "+TheText;



                g = DrawABox(g, x0.intValue(),y0.intValue(),Width.intValue(), Height.intValue(), Label,BoundingBoxHexColor,LineThickness, LabelBoxHexColor, LabelTextColor);

            }


            return result;
        }

    public static Graphics2D DrawABox(Graphics2D g, int x0,int y0,int width, int height, String Label, String BoundingBoxHexColor, Number LineThickness, String LabelBoxHexColor,String LabelTextColor){
        // Bounding Box around Object
        g.setColor(hex2Rgb(BoundingBoxHexColor));
        g.drawRect(x0, y0, width+2, height+2);

        // Bounding Box around Label

        g.setColor(hex2Rgb(LabelBoxHexColor));
        int LabelRectLength = g.getFontMetrics().stringWidth(Label)+8;
        g.fillRect(x0-LabelRectLength, y0, LabelRectLength, height+2);

        float thickness = LineThickness.floatValue();
        Stroke stroke = new BasicStroke(thickness);
        g.setStroke(stroke);

        // Color of Text
        g.setColor(hex2Rgb(LabelTextColor));
        g.drawRect(x0-LabelRectLength, y0, LabelRectLength, height+2);
        g.drawString(Label, x0-LabelRectLength+4, y0+height-1);

        return g;

    }

    public static BufferedImage drawObjectsV8(BufferedImage img,String JsonInput) {
        Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
        BufferedImage result;
            try{
                result = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
            }catch(IllegalArgumentException e){
                throw new BotCommandException(MESSAGES.getString("wrongImageFormat"));
            }

        JSONParser parser = new JSONParser();
        try {
            JSONObject jobj = (JSONObject) parser.parse(JsonInput);

            try{
                JSONObject DocMetadata = (JSONObject) jobj.get("DocumentMetadata");
                //System.out.println("DEBUG:"+DocMetadata.toString());
                int StatusCode = (int)DocMetadata.get("Status");
                if(StatusCode == 500){
                    String Error = (String)jobj.get("Error");
                    throw new BotCommandException(MESSAGES.getString("v8Error",Error));
                }
            }catch(Exception e){

                JSONArray AllBlocks = (JSONArray) jobj.get("infoSet");
                Blocks blocks = new Blocks(AllBlocks);
                result = blocks.drawBlocks(img,blocks.AllBlocksList);

                JSONArray AllKeyValuePairs = (JSONArray) jobj.get("keyValueSet");
                KeyValuePairs pairs = new KeyValuePairs(AllKeyValuePairs);
                result = pairs.drawPairs(result,pairs.AllPairsList);

                JSONArray AllTables = (JSONArray) jobj.get("tableSet");
                TableSets tables = new TableSets(AllTables);
                result = tables.drawTables(result,tables.AllTables);

            }





        }catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static BufferedImage drawTables(BufferedImage img,String JsonInput) {
        Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
        BufferedImage result;
        try{
            result = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        }catch(IllegalArgumentException e){
            throw new BotCommandException(MESSAGES.getString("wrongImageFormat"));
        }

        JSONParser parser = new JSONParser();
        try {
            JSONObject jobj = (JSONObject) parser.parse(JsonInput);
            JSONArray AllTables = (JSONArray) jobj.get("TableSet");
            TableSets tables = new TableSets(AllTables);
            result = tables.drawTables(result,tables.AllTables);


        }catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }


        public static BufferedImage drawLabel(BufferedImage img, String LabelName, String LabelValue, String HexColor, Number X, Number Y, Number FontSize) {

            BufferedImage result = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
            //Graphics g = result.getGraphics();
            Graphics2D g = result.createGraphics();
            g.drawImage(img, 0, 0, null);

            g.setColor(hex2Rgb(HexColor));

            String Label = LabelName+"  "+LabelValue;
            // LabelRectLength = g.getFontMetrics().stringWidth(Label)+8;

           // g.fillRect(x0-LabelRectLength, y0, LabelRectLength, height+2);

            Number Thickness = 2;
            float thickness = Thickness.floatValue();
            Stroke stroke = new BasicStroke(thickness);

            g.setStroke(stroke);
            g.setFont(new Font("TimesRoman", Font.PLAIN, FontSize.intValue()));

            g.drawString(Label,X.floatValue(),Y.floatValue());

            return result;
        }



        public static Color hex2Rgb(String colorStr) {
            return new Color(
                    Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
                    Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
                    Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) );
        }


        private static void bgr2rgb(byte[] data) {
            for (int i = 0; i < data.length; i += 3) {
                byte tmp = data[i];
                data[i] = data[i + 2];
                data[i + 2] = tmp;
            }
        }


        public static double round(double value, int places) {
            if (places < 0) throw new IllegalArgumentException();

            BigDecimal bd = BigDecimal.valueOf(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        }



}

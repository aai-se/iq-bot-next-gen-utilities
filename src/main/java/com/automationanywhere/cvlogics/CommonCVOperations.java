package com.automationanywhere.cvlogics;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.photo.Photo;

import java.text.DecimalFormat;
import java.util.*;

public class CommonCVOperations {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    public static void RemoveColorRange(String PathToINPUTImageFile, String PathToOUTPUTImageFile, Double MinRed,Double MaxRed, Double MinBlue, Double MaxBlue, Double MinGreen, Double MaxGreen){
        try {
            Mat imageAsMatrix = Imgcodecs.imread(PathToINPUTImageFile, 1); //IMREAD_COLOR

            // ORDER IS BLUE, GREEN, RED
            Scalar ScalarLower = new Scalar(MinBlue, MinGreen, MinRed);
            Scalar ScalarHigher = new Scalar(MaxBlue, MaxGreen, MaxRed);
            Mat ImageProcessed = new Mat();

            Core.inRange(imageAsMatrix, ScalarLower, ScalarHigher, ImageProcessed);

            Imgcodecs.imwrite(PathToOUTPUTImageFile, ImageProcessed);
        }catch(Exception e){
            throw new BotCommandException(MESSAGES.getString("openCVOpenCIssue",PathToINPUTImageFile));
        }
    }

    public static void GrayscaleImage(String PathToINPUTImageFile, String PathToOUTPUTImageFile){
        Mat imageAsMatrix = Imgcodecs.imread(PathToINPUTImageFile);
        Imgproc.cvtColor(imageAsMatrix,imageAsMatrix, Imgproc.COLOR_BGR2GRAY);
        Imgcodecs.imwrite(PathToOUTPUTImageFile,imageAsMatrix);
    }

    public static void CropImage(String PathToINPUTImageFile, String PathToOUTPUTImageFile, int x, int y, int width, int height){
        Mat imageAsMatrix = Imgcodecs.imread(PathToINPUTImageFile);
        Rect rect_Crop = new Rect(x, y, width, height);
        Mat image_processed = new Mat(imageAsMatrix,rect_Crop);
        Imgcodecs.imwrite(PathToOUTPUTImageFile,image_processed);
    }

    public static void FlipImage(String PathToINPUTImageFile, String PathToOUTPUTImageFile, boolean HoriFlip,boolean VertFlip){
        Mat imageAsMatrix = Imgcodecs.imread(PathToINPUTImageFile);
        if(HoriFlip && VertFlip){
            Core.flip(imageAsMatrix, imageAsMatrix, -1);
        }else if (HoriFlip && !VertFlip){
            Core.flip(imageAsMatrix, imageAsMatrix, 1);
        }else if(!HoriFlip && VertFlip){
            Core.flip(imageAsMatrix, imageAsMatrix, 0);
        }

        Imgcodecs.imwrite(PathToOUTPUTImageFile,imageAsMatrix);
    }

//ResizeImageWithPercentage
public static void ResizeImageWithPercentage(String PathToINPUTImageFile, String PathToOUTPUTImageFile, Double newWidthPercentage, Double newHeightPercentage){
    Mat imageAsMatrix = Imgcodecs.imread(PathToINPUTImageFile);
    Size oriSize = imageAsMatrix.size();

    if(newWidthPercentage == null){
        newWidthPercentage = 100.0;
    }

    if(newHeightPercentage == null){
        newHeightPercentage = 100.0;
    }

    Double NewHeight = oriSize.height * newHeightPercentage / 100;
    Double NewWidth = oriSize.width * newWidthPercentage / 100;

    Size newSize = new Size(NewWidth,NewHeight);

    Imgproc.resize( imageAsMatrix, imageAsMatrix, newSize );
    Imgcodecs.imwrite(PathToOUTPUTImageFile,imageAsMatrix);
}

    public static void ResizeImage(String PathToINPUTImageFile, String PathToOUTPUTImageFile, Double newWidth, Double newHeight){
        Mat imageAsMatrix = Imgcodecs.imread(PathToINPUTImageFile);
        Size oriSize = imageAsMatrix.size();
        Size newSize = new Size(oriSize.width,oriSize.height);

        if(newWidth != null && newHeight != null){
            newSize = new Size(newWidth,newHeight);
        }

        if(newWidth == null && newHeight != null){
            newSize = new Size(oriSize.width,newHeight);
        }

        if(newWidth != null && newHeight == null){
            newSize = new Size(newWidth,oriSize.width);
        }

        Imgproc.resize( imageAsMatrix, imageAsMatrix, newSize );
        Imgcodecs.imwrite(PathToOUTPUTImageFile,imageAsMatrix);
    }

    public static void RotateImage(String PathToINPUTImageFile, String PathToOUTPUTImageFile, double Angle, double ScalingFactor){
        Mat imageAsMatrix = Imgcodecs.imread(PathToINPUTImageFile);
        Size imSize = imageAsMatrix.size();
        Double CenterH = imSize.height / 2;
        Double CenterW = imSize.width / 2;

        Mat rotationMatrix = Imgproc.getRotationMatrix2D(new Point(CenterH, CenterW), Angle, ScalingFactor);

        //Rotating the given image
        Scalar BorderValue = new Scalar(255,255.255);

        Imgproc.warpAffine(imageAsMatrix, imageAsMatrix,rotationMatrix, new Size(imageAsMatrix.cols(), imageAsMatrix.cols()),0,1,BorderValue);

        Imgcodecs.imwrite(PathToOUTPUTImageFile,imageAsMatrix);
    }

    public static boolean ConcatenateImages(String Layout, String ImageFile1, String ImageFile2,String PathToOUTPUTImageFile){

        int top, bottom, left, right;
        int borderType = Core.BORDER_CONSTANT;
        Scalar colorvalue = new Scalar( 255, 255, 255 );

        Mat image1 = Imgcodecs.imread(ImageFile1);
        Mat image2 = Imgcodecs.imread(ImageFile2);

        if (image1.channels() == 1) Imgproc.cvtColor(image1, image1, Imgproc.COLOR_BGR2RGB);
        if (image1.channels() == 1) Imgproc.cvtColor(image2, image2, Imgproc.COLOR_BGR2RGB);

        int deltacols = image1.cols()-image2.cols();
        int deltarows = image1.rows()-image2.rows();

        left =  Math.abs(deltacols/2);
        right = Math.abs(deltacols/2) + Math.abs(deltacols%2);
        top = Math.abs(deltarows/2);
        bottom = Math.abs(deltarows/2) + Math.abs(deltarows%2);

        Mat dst = new Mat();
        if (Layout.toUpperCase().equals("VERTICAL")) {
            if (deltacols > 0) {
                Core.copyMakeBorder( image2, image2, 0,0, left, right, borderType, colorvalue);
            }
            else
            {
                Core.copyMakeBorder( image1, image1, 0,0, left, right, borderType, colorvalue);
            }
            List<Mat> src = Arrays.asList(image1,image2);
            Core.vconcat(src,dst);
        }
        else {
            if (deltarows > 0) {
                Core.copyMakeBorder( image2, image2, top,bottom,0,0, borderType, colorvalue);
            }
            else
            {
                Core.copyMakeBorder( image1, image1, top,bottom,0,0, borderType, colorvalue);
            }
            List<Mat> src = Arrays.asList(image1,image2);
            Core.hconcat(src,dst);
        }

        Imgcodecs.imwrite(PathToOUTPUTImageFile, dst);
        return true;
    }

    public static void Denoise(String PathToINPUTImageFile, String PathToOUTPUTImageFile, Double Filter){
        Mat image = Imgcodecs.imread(PathToINPUTImageFile);
        Imgproc.cvtColor(image,image,Imgproc.COLOR_BGR2GRAY);
        Mat imagenoise = Mat.zeros(image.size(), image.type());

        Photo.fastNlMeansDenoising(image, imagenoise ,Filter.floatValue());
        Imgcodecs.imwrite(PathToOUTPUTImageFile, imagenoise);

    }

    private static double ratio(Mat image) {
        return image.size().height/image.size().width;
    }

    private static byte saturate(double val) {
        int iVal = (int) Math.round(val);
        iVal = iVal > 255 ? 255 : (iVal < 0 ? 0 : iVal);
        return (byte) iVal;
    }

    public static void ChangeContrastAndBrightness(String PathToINPUTImageFile, String PathToOUTPUTImageFile, Double ContrastLevel, Double BrightnessLevel){
        Mat image = Imgcodecs.imread(PathToINPUTImageFile);


        Mat imagenew = Mat.zeros(image.size(), image.type());
        Mat newImage = Mat.zeros(image.size(), image.type());
        double alpha = ContrastLevel.doubleValue();
        int beta = BrightnessLevel.intValue()  ;

        byte[] imageData = new byte[(int) (image.total()*image.channels())];
        image.get(0, 0, imageData);
        byte[] newImageData = new byte[(int) (newImage.total()*newImage.channels())];
        for (int y = 0; y < image.rows(); y++) {
            for (int x = 0; x < image.cols(); x++) {
                for (int c = 0; c < image.channels(); c++) {
                    double pixelValue = imageData[(y * image.cols() + x) * image.channels() + c];
                    pixelValue = pixelValue < 0 ? pixelValue + 256 : pixelValue;
                    newImageData[(y * image.cols() + x) * image.channels() + c]
                            = saturate(alpha * pixelValue + beta);
                }
            }
        }

        imagenew.put(0, 0, newImageData);
        Imgcodecs.imwrite(PathToOUTPUTImageFile, imagenew);

    }
    public static void SimpleThreshold(String PathToINPUTImageFile, String PathToOUTPUTImageFile,int mSize, int threshold, int MaxValue,boolean INV){
        Mat imageAsMatrix = Imgcodecs.imread(PathToINPUTImageFile);
        // Grayscale
        Imgproc.cvtColor(imageAsMatrix,imageAsMatrix, Imgproc.COLOR_BGR2GRAY);
        // Gaussian Blur
        Size mgSize = new Size(mSize,mSize);
        Imgproc.GaussianBlur(imageAsMatrix,imageAsMatrix,mgSize,0);
        // Thresholding
        if(INV){
            Imgproc.threshold(imageAsMatrix,imageAsMatrix,threshold,MaxValue,Imgproc.THRESH_BINARY_INV);
        }else{
            Imgproc.threshold(imageAsMatrix,imageAsMatrix,threshold,MaxValue,Imgproc.THRESH_BINARY);
        }

        Imgcodecs.imwrite(PathToOUTPUTImageFile,imageAsMatrix);
    }

    public static void AdaptiveThreshold(String PathToINPUTImageFile, String PathToOUTPUTImageFile,int mSize, int MaxValue,boolean INV,boolean Blur){
        Mat imageAsMatrix = Imgcodecs.imread(PathToINPUTImageFile);
        // Grayscale
        Imgproc.cvtColor(imageAsMatrix,imageAsMatrix, Imgproc.COLOR_BGR2GRAY);
        if(Blur){
            // Gaussian Blur
            Size mgSize = new Size(mSize,mSize);
            Imgproc.GaussianBlur(imageAsMatrix,imageAsMatrix,mgSize,0);

        }
       // thresh_mean = cv2.adaptiveThreshold(blurred, pixel_max_value,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 11, 4)
        //thresh_gaussian = cv2.adaptiveThreshold(blurred, pixel_max_value,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 29, 3)

        // Thresholding
        if(INV){
            Imgproc.adaptiveThreshold(imageAsMatrix,imageAsMatrix,MaxValue,Imgproc.ADAPTIVE_THRESH_MEAN_C,Imgproc.THRESH_BINARY_INV,11,0);
        }else{
            Imgproc.adaptiveThreshold(imageAsMatrix,imageAsMatrix,MaxValue,Imgproc.ADAPTIVE_THRESH_MEAN_C,Imgproc.THRESH_BINARY,11,0);
        }

        Imgcodecs.imwrite(PathToOUTPUTImageFile,imageAsMatrix);
    }

    public static String GetBlur(String PathToINPUTImageFile){

        //def variance_of_laplacian(image):
        //	# compute the Laplacian of the image and then return the focus
        //	# measure, which is simply the variance of the Laplacian
        //	return cv2.Laplacian(image, cv2.CV_64F).var()
        //fm = variance_of_laplacian(gray)


        Mat destination = new Mat();
        Mat matGray=new Mat();
        Mat imageAsMatrix = Imgcodecs.imread(PathToINPUTImageFile);

        Imgproc.cvtColor(imageAsMatrix, matGray, Imgproc.COLOR_BGR2GRAY);
        Imgproc.Laplacian(matGray, destination, 3);
        MatOfDouble median = new MatOfDouble();
        MatOfDouble std= new MatOfDouble();
        Core.meanStdDev(destination, median , std);

        double myVariance = Math.pow(std.get(0, 0)[0], 2);
        DecimalFormat df = new DecimalFormat("#####");
        return df.format(myVariance);

       // return myVariance;
    }

    public static void Blur(String PathToINPUTImageFile, String PathToOUTPUTImageFile,int mSize){
        Mat imageAsMatrix = Imgcodecs.imread(PathToINPUTImageFile);
        // Gaussian Blur
        Size mgSize = new Size(mSize,mSize);
        Imgproc.GaussianBlur(imageAsMatrix,imageAsMatrix,mgSize,0);
        Imgcodecs.imwrite(PathToOUTPUTImageFile,imageAsMatrix);
    }

    public static void Sharpen(String PathToINPUTImageFile, String PathToOUTPUTImageFile, Number sigma, Number alpha, Number beta, Number gamma){
        Mat imageAsMatrix = Imgcodecs.imread(PathToINPUTImageFile);
        Mat imagebw = Mat.zeros(imageAsMatrix.size(), imageAsMatrix.type());
        Mat imagenew = Mat.zeros(imageAsMatrix.size(), imageAsMatrix.type());
        Imgproc.cvtColor(imageAsMatrix, imagebw, Imgproc.COLOR_BGR2GRAY);
        Imgproc.GaussianBlur(imagebw, imagenew, new Size(0, 0), sigma.doubleValue());
        Core.addWeighted(imagebw, alpha.doubleValue(), imagenew, beta.doubleValue(), gamma.doubleValue(),imagenew);
        Imgcodecs.imwrite(PathToOUTPUTImageFile, imagenew);

    }

    public static void CropAuto(String PathToINPUTImageFile, String PathToOUTPUTImageFile, Number threshold){
        Mat image = Imgcodecs.imread(PathToINPUTImageFile);

        Mat imagebw = Mat.zeros(image.size(), image.type());
        Mat imagenew = Mat.zeros(image.size(), image.type());

        // Grayscale
        Imgproc.cvtColor(image, imagebw, Imgproc.COLOR_BGR2GRAY);
        // Threshold (static)
        Imgproc.threshold(imagebw, imagebw, threshold.doubleValue(),255, Imgproc.THRESH_BINARY_INV);

        // Close Kernel Operation of Ellipse
        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(11,11));
        Imgproc.morphologyEx(imagebw, imagebw, Imgproc.MORPH_CLOSE, kernel);

        MatOfPoint maxContour = null ;
        Mat hierarchy = new Mat();
        double maxVal = 0;
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(imagebw, contours,hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++)
        {
            double contourArea = Imgproc.contourArea(contours.get(contourIdx));
            if (maxVal < contourArea)
            {
                maxVal = contourArea;
                maxContour = contours.get(contourIdx);
            }
        }

        Rect boundingrect = Imgproc.boundingRect(new MatOfPoint(maxContour.toArray()));
        imagenew = image.submat(boundingrect);

        Imgcodecs.imwrite(PathToOUTPUTImageFile, imagenew);
    }

    public static Map<String, Value> DetectObjectsFromXML(String PathToINPUTImageFile, String PathToOUTPUTImageFile, String PathToXMLFile, Number MaxNumOfObjs){

        //HashMap<String,String> Results = new HashMap<String,String>();
        Map<String, Value> Results = new LinkedHashMap();
        //NumberValue returnvalue = new NumberValue();

        int limit = (int) ((MaxNumOfObjs != null) ? MaxNumOfObjs.longValue() : 100);

        Mat image = Imgcodecs.imread(PathToINPUTImageFile);

        Mat imagebw = Mat.zeros(image.size(), image.type());
        Mat imagenew = Mat.zeros(image.size(), image.type());

        imagenew = image.clone();
        Imgproc.cvtColor(image, image, Imgproc.COLOR_BGR2GRAY);
        // Instantiating the CascadeClassifier

        CascadeClassifier classifier = new CascadeClassifier(PathToXMLFile);

        // Detecting the face in the snap
        MatOfRect objectDetections = new MatOfRect();
        classifier.detectMultiScale(image, objectDetections);
        PriorityQueue<Rect> rq = new PriorityQueue<Rect>(5,(r1, r2) -> {
            if (r1.area() < r2.area())
                return 1;
            else if (r1.area() > r2.area())
                return -1;
            return 0;
        }
        );

        for (Rect rect : objectDetections.toArray()) {
            rq.add(rect);
        }

        String TotalNumberOfObjects = Integer.toString(objectDetections.toArray().length);

        Results.put("number",new StringValue(TotalNumberOfObjects));

        // Drawing boxes
        Integer objCounter = 0;
        for (int i= 0;i<limit && i < objectDetections.toArray().length ;i++) {
            objCounter++;
            Rect rect = rq.poll();
            String BoundingBox = rect.x+":"+rect.y+":"+rect.width+":"+rect.height;
            Results.put(Integer.toString(objCounter),new StringValue(BoundingBox));

            Imgproc.rectangle(
                    imagenew,                                               // where to draw the box
                    new Point(rect.x, rect.y),                            // bottom left
                    new Point(rect.x + rect.width, rect.y + rect.height), // top right
                    new Scalar(0, 0, 255),
                    3                                                     // RGB colour
            );
        }
        if(!PathToOUTPUTImageFile.equals("")) {
            Imgcodecs.imwrite(PathToOUTPUTImageFile, imagenew);
        }

    return Results;

    }

}

package com.automationanywhere.cvlogics;

import com.automationanywhere.V8.Blocks;
import com.automationanywhere.V8.KeyValuePairs;
import com.automationanywhere.V8.TableSets;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class CommonConversionOperations {

    public static boolean ConvertToCsv(String OutputFilePathRoot, String JsonInput) throws IOException, ParseException {
        Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

        String OutputFilePathStd = OutputFilePathRoot+"_STD.csv";

        FileWriter writerStd = new FileWriter(OutputFilePathStd);


        ArrayList<String> CsvStringsStd = new ArrayList<>();
        ArrayList<String> CsvStringsTable = new ArrayList<>();

        JSONParser parser = new JSONParser();
        JSONObject jobj = (JSONObject) parser.parse(JsonInput);

            try{
                JSONObject DocMetadata = (JSONObject) jobj.get("DocumentMetadata");
                //System.out.println("DEBUG:"+DocMetadata.toString());
                int StatusCode = (int)DocMetadata.get("Status");
                if(StatusCode == 500){
                    String Error = (String)jobj.get("Error");
                    throw new BotCommandException(MESSAGES.getString("v8Error",Error));
                }
            }catch(Exception e) {
                String Headers = "BlockType,InfoBlockType,BlockID,BlockText,BlockKey,BlockValue,DetectionConfidence,OCRConfidence,ExtractionScore,x,y,width,height";
                CsvStringsStd.add(Headers);

                JSONArray AllBlocks = (JSONArray) jobj.get("infoSet");
                Blocks blocks = new Blocks(AllBlocks);
                ArrayList<String> BlockInfos = blocks.getCSV(blocks.AllBlocksList);
                CsvStringsStd.addAll(BlockInfos);

                JSONArray AllKeyValuePairs = (JSONArray) jobj.get("keyValueSet");
                KeyValuePairs pairs = new KeyValuePairs(AllKeyValuePairs);
                ArrayList<String> KVPairInfos = pairs.getCSV(pairs.AllPairsList);
                CsvStringsStd.addAll(KVPairInfos);

                // Writing CSV for Standard Data
                for (String s : CsvStringsStd) {
                    writerStd.write(s + "\n");
                }
                writerStd.close();
                // for(String s:CsvStringsStd){
                //     System.out.println(s);
                //}

                JSONArray AllTables = (JSONArray) jobj.get("tableSet");
                TableSets tables = new TableSets(AllTables);
                ArrayList<ArrayList<String>> AllDataFromTables = tables.getCSVs(",");


                // result = tables.drawTables(result,tables.AllTables);
                if (AllDataFromTables.size() > 0) {

                    for (int i = 0; i < AllDataFromTables.size(); i++) {
                        ArrayList<String> tabledata = AllDataFromTables.get(i);
                        int TableIdx = i + 1;
                        String OutputFilePathTable = OutputFilePathRoot + "_TABLE_" + TableIdx + ".csv";
                        FileWriter writerTable = new FileWriter(OutputFilePathTable);
                        for (String s : tabledata) {
                            writerTable.write(s + "\n");
                        }
                        writerTable.close();
                    }

                }
            }

        return true;
    }

}

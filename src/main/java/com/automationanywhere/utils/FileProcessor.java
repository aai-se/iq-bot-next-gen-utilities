package com.automationanywhere.utils;

import org.apache.commons.io.FilenameUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;


public class FileProcessor {

    private String[] SupportedImageExtensions = {"tif", "tiff", "png","jpeg","jpg"};
    private String[] SupportedPdfExtensions = {"pdf"};
    public boolean IsPDF = false;
    public boolean IsImage = false;
    private String FILEPATH = "";
    public String TempFolderUUID = "";
    public String TEMPROOTFOLDER = "C:\\ProgramData\\AutomationAnywhere\\ImageProcessing";
    public String FullTempFilePath = "";
    //

    public FileProcessor(){}
    public FileProcessor(String PathToImageOrPdf){
        this.FILEPATH = PathToImageOrPdf;
        this.IsPDF = isPDF(getFileExtension(this.FILEPATH));
        this.IsImage = isImage(getFileExtension(this.FILEPATH));
        this.TempFolderUUID = UUID.randomUUID().toString();
        CheckOrCreateFolder(this.TEMPROOTFOLDER);
        Path TempFilePath = Paths.get(this.TEMPROOTFOLDER, this.TempFolderUUID);
        this.FullTempFilePath = TempFilePath.toString();
        boolean WasCreated = CheckOrCreateFolder(TempFilePath.toString());
    }

    void deleteDirectoryRecursion(Path path) throws IOException {
        if (Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS)) {
            try (DirectoryStream<Path> entries = Files.newDirectoryStream(path)) {
                for (Path entry : entries) {
                    deleteDirectoryRecursion(entry);
                }
            }
        }
        Files.delete(path);
    }

    public boolean Cleanup(){
        Path path = Paths.get(this.FullTempFilePath);
        try{
            deleteDirectoryRecursion(path);
        }catch(IOException e){
            return false;
        }
        return true;
    }

    public boolean CheckOrCreateFolder(String MyFolder){
        File directory = new File(MyFolder);
        if (! directory.exists()){
            directory.mkdirs();
            return true;
        }else{
            return false;
        }
    }

    public static PDDocument getFileAsPDFDocument(String FilePath){
        //Loading an existing PDF document
        PDDocument document = null;
        try{
            File file = new File(FilePath);
            document = PDDocument.load(file);
        }catch(IOException e){
            return null;
        }
        return document;
    }

    public ArrayList<String> GetFileAsImages(){

        ArrayList<String> AllImages = new ArrayList<String>();
        if(this.IsPDF){
            PDDocument pFile = getFileAsPDFDocument(this.FILEPATH);
            if(pFile == null){return AllImages;}

            PDFRenderer pdfRenderer = new PDFRenderer(pFile);
            for (int page = 0; page < pFile.getNumberOfPages(); ++page)
            {
                try{
                    BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);
                    Path TempImage = Paths.get(this.TEMPROOTFOLDER, this.TempFolderUUID,"page_"+page+".png");
                    AllImages.add(TempImage.toString());
                    ImageIO.write(bim, "png", new File(TempImage.toString()));
                }catch(IOException e){
                    // TBD
                }

            }
            try{
                pFile.close();
            }catch(IOException e){
                // TBD
            }
        }else{
            AllImages.add(this.FILEPATH);
        }
        return AllImages;
    }



    private static boolean addImageAsNewPage(PDDocument doc, String imagePath, PDRectangle PageSize) {
        try {
            PDImageXObject image          = PDImageXObject.createFromFile(imagePath, doc);
            PDRectangle    pageSize       = PageSize;

            int            originalWidth  = image.getWidth();
            int            originalHeight = image.getHeight();
            float          pageWidth      = pageSize.getWidth();
            float          pageHeight     = pageSize.getHeight();
            float          ratio          = Math.min(pageWidth / originalWidth, pageHeight / originalHeight);
            float          scaledWidth    = originalWidth  * ratio;
            float          scaledHeight   = originalHeight * ratio;
            float          x              = (pageWidth  - scaledWidth ) / 2;
            float          y              = (pageHeight - scaledHeight) / 2;

            PDPage         page           = new PDPage(pageSize);
            doc.addPage(page);
            try (PDPageContentStream contents = new PDPageContentStream(doc, page)) {
                contents.drawImage(image, x, y, scaledWidth, scaledHeight);
            }
            //System.out.println("Added: " + imagePath);
        } catch (IOException e) {
            //System.err.println("Failed to process: " + imagePath);
            e.printStackTrace(System.err);
            return false;
        }
        return true;
    }

    public boolean CombineImagesToPdf(ArrayList<String> ListOfTempProcessedImages, String OutputFilePath){
        PDDocument doc = new PDDocument();
        for (String image : ListOfTempProcessedImages){
            PDRectangle pageSize = PDRectangle.A4;
            boolean res = addImageAsNewPage(doc, image,pageSize);
            if(!res){
                return false;
            }
        }
        try{
            doc.save(OutputFilePath);
        }catch(IOException e){
            // TBD

            try{
                doc.close();
            }catch(IOException e1){
                return false;
            }

            return false;
        }
        try{
            doc.close();
        }catch(IOException e1){
            return false;
        }
        return true;
    }

    public String getFileExtension(String filename) {
        return FilenameUtils.getExtension(filename);
    }

    public boolean isImage(String extension){
        return Arrays.stream(SupportedImageExtensions).parallel().anyMatch(extension::contains);
    }

    public boolean isPDF(String extension){
        return Arrays.stream(SupportedPdfExtensions).parallel().anyMatch(extension::contains);
    }

    public boolean isExtensionSupported(String extension){
            boolean isImageExt = isImage(extension);
            boolean isPdfExt = isPDF(extension);
            if(isImageExt || isPdfExt){return true;}
            return false;
    }

    public boolean isFileAPdf(){
        return isFileAPdf(this.FILEPATH);
    }

    public boolean isFileAPdf(String FilePath){
        if(isPDF(getFileExtension(FilePath))){
            return true;
        }
        return false;
    }

    public boolean isFileAnImage(){
        return isFileAnImage(this.FILEPATH);
    }

    public boolean isFileAnImage(String FilePath){
        if(isImage(getFileExtension(FilePath))){
            return true;
        }
        return false;
    }
}

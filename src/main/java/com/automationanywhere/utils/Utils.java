package com.automationanywhere.utils;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Utils {

    public static String FileToString(String filePath)
        {
            String content = "";
            try {
                // default StandardCharsets.UTF_8
                content = Files.readString(Paths.get(filePath), StandardCharsets.ISO_8859_1);

                //System.out.println(content);

            } catch (IOException e) {
                e.printStackTrace();
                return "";
            } catch(InvalidPathException e1){
                e1.printStackTrace();
                return "";
            }
            //System.out.println(content);
            return content;
        }

        public static String ConvertScoreToHumanReadable(double RawConfidenceScore){
            RawConfidenceScore = RawConfidenceScore * 100;
            double RoundedScore = round(RawConfidenceScore, 2);
            return Double.toString(RoundedScore);
        }

        public static double round(double value, int places) {
            if (places < 0) throw new IllegalArgumentException();

            BigDecimal bd = BigDecimal.valueOf(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        }



}

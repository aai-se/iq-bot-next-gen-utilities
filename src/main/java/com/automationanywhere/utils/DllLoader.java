package com.automationanywhere.utils;


import org.apache.commons.io.FileUtils;
import org.opencv.core.Core;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class DllLoader {
    public DllLoader(){
        // Should test for existence of file..
        //System.load("C:\\OpenCV_AA\\opencv\\build\\java\\x64\\opencv_java420.dll");
        String LibFileName = "opencv-420.jar";
        String PathToJavaBin = System.getProperty("java.home");
        //System.out.println("DEBUG:"+PathToJavaBin);
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        //System.out.println(Core.NATIVE_LIBRARY_NAME);

        String path = "libs/"+LibFileName;
        File file = new File(path);
        String OpenCVLibAbsPath = file.getAbsolutePath();
        File OpenCVLib = new File(OpenCVLibAbsPath);
        //String path= getClass().getResource("/libs/opencv-420.jar").getPath();
        //System.out.println("DEBUG1:"+OpenCVLibAbsPath);
        File JavaLibInBinFolder = new File(PathToJavaBin+"/bin/"+LibFileName);

        if(!JavaLibInBinFolder.exists()) {
            try {
                FileUtils.copyFile(OpenCVLib, JavaLibInBinFolder);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    public static boolean copy(InputStream source , String destination) {
        boolean succeess = true;

        System.out.println("Copying ->" + source + "\tto ->" + destination);

        try {
            Files.copy(source, Paths.get(destination), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            //logger.log(Level.WARNING, "", ex);
            succeess = false;
        }

        return succeess;

    }
}

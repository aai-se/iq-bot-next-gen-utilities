package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class JSONToCSVCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(JSONToCSVCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    JSONToCSV command = new JSONToCSV();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("JsonInput") && parameters.get("JsonInput") != null && parameters.get("JsonInput").get() != null) {
      convertedParameters.put("JsonInput", parameters.get("JsonInput").get());
      if(!(convertedParameters.get("JsonInput") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","JsonInput", "String", parameters.get("JsonInput").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("JsonInput") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","JsonInput"));
    }

    if(parameters.containsKey("InputIsJSONFile") && parameters.get("InputIsJSONFile") != null && parameters.get("InputIsJSONFile").get() != null) {
      convertedParameters.put("InputIsJSONFile", parameters.get("InputIsJSONFile").get());
      if(!(convertedParameters.get("InputIsJSONFile") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InputIsJSONFile", "Boolean", parameters.get("InputIsJSONFile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("InputIsJSONFile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","InputIsJSONFile"));
    }

    if(parameters.containsKey("OutputFilePathRoot") && parameters.get("OutputFilePathRoot") != null && parameters.get("OutputFilePathRoot").get() != null) {
      convertedParameters.put("OutputFilePathRoot", parameters.get("OutputFilePathRoot").get());
      if(!(convertedParameters.get("OutputFilePathRoot") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","OutputFilePathRoot", "String", parameters.get("OutputFilePathRoot").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("OutputFilePathRoot") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","OutputFilePathRoot"));
    }

    command.setSessions(sessionMap);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("JsonInput"),(Boolean)convertedParameters.get("InputIsJSONFile"),(String)convertedParameters.get("OutputFilePathRoot")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}

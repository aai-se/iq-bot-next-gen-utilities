package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class DrawObjectsMSFormRecognizerCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(DrawObjectsMSFormRecognizerCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    DrawObjectsMSFormRecognizer command = new DrawObjectsMSFormRecognizer();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("DictionaryList") && parameters.get("DictionaryList") != null && parameters.get("DictionaryList").get() != null) {
      convertedParameters.put("DictionaryList", parameters.get("DictionaryList").get());
      if(!(convertedParameters.get("DictionaryList") instanceof List)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","DictionaryList", "List", parameters.get("DictionaryList").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("DictionaryList") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","DictionaryList"));
    }

    if(parameters.containsKey("InputFilePath") && parameters.get("InputFilePath") != null && parameters.get("InputFilePath").get() != null) {
      convertedParameters.put("InputFilePath", parameters.get("InputFilePath").get());
      if(!(convertedParameters.get("InputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InputFilePath", "String", parameters.get("InputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("InputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","InputFilePath"));
    }

    if(parameters.containsKey("OutputFilePath") && parameters.get("OutputFilePath") != null && parameters.get("OutputFilePath").get() != null) {
      convertedParameters.put("OutputFilePath", parameters.get("OutputFilePath").get());
      if(!(convertedParameters.get("OutputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","OutputFilePath", "String", parameters.get("OutputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("OutputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","OutputFilePath"));
    }

    if(parameters.containsKey("BoundingBoxHexColor") && parameters.get("BoundingBoxHexColor") != null && parameters.get("BoundingBoxHexColor").get() != null) {
      convertedParameters.put("BoundingBoxHexColor", parameters.get("BoundingBoxHexColor").get());
      if(!(convertedParameters.get("BoundingBoxHexColor") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","BoundingBoxHexColor", "String", parameters.get("BoundingBoxHexColor").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("LineThickness") && parameters.get("LineThickness") != null && parameters.get("LineThickness").get() != null) {
      convertedParameters.put("LineThickness", parameters.get("LineThickness").get());
      if(!(convertedParameters.get("LineThickness") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","LineThickness", "Number", parameters.get("LineThickness").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("LabelBoxHexColor") && parameters.get("LabelBoxHexColor") != null && parameters.get("LabelBoxHexColor").get() != null) {
      convertedParameters.put("LabelBoxHexColor", parameters.get("LabelBoxHexColor").get());
      if(!(convertedParameters.get("LabelBoxHexColor") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","LabelBoxHexColor", "String", parameters.get("LabelBoxHexColor").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("LabelTextColor") && parameters.get("LabelTextColor") != null && parameters.get("LabelTextColor").get() != null) {
      convertedParameters.put("LabelTextColor", parameters.get("LabelTextColor").get());
      if(!(convertedParameters.get("LabelTextColor") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","LabelTextColor", "String", parameters.get("LabelTextColor").get().getClass().getSimpleName()));
      }
    }

    command.setSessions(sessionMap);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((List<Value>)convertedParameters.get("DictionaryList"),(String)convertedParameters.get("InputFilePath"),(String)convertedParameters.get("OutputFilePath"),(String)convertedParameters.get("BoundingBoxHexColor"),(Number)convertedParameters.get("LineThickness"),(String)convertedParameters.get("LabelBoxHexColor"),(String)convertedParameters.get("LabelTextColor")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
